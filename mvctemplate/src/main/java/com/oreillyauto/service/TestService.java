package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Test;

public interface TestService {
    public List<Test> getTest();
}
